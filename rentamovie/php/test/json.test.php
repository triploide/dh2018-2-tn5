<?php

$db = [
	'peliculas' => [
		[
			'titulo' => 'Avatar',
			'premios' => 7
		],
		[
			'titulo' => 'Terminator',
			'premios' => 3
		]
	],
	'usuarios' => [
		[
			'nombre' => 'Bort',
			'apellido' => 'Bort',
			'email' => 'bort@mail.com',
			'pass' => '123456'
		],
		[
			'nombre' => 'Bart',
			'apellido' => 'Bart',
			'email' => 'bart@mail.com',
			'pass' => '123456'
		]
	]
];

$db = json_encode($db);

file_put_contents('db.json', $db);

