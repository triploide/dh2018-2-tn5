<?php

abstract class Modelo
{
	protected $base;
	protected $key;

	public function __construct()
	{
		$this->base = new JSON;
	}

	public function seleccionar($attr, $valor)
	{
		return $this->base->seleccionar($attr, $valor, $this);
	}

	public function meterAtributos($datos)
	{
		//meter setters
		foreach ($datos as $key => $dato) {
			$this->setear($key, $dato);
		}
		return $this;
	}

	public function setear($key, $value)
	{
		$this->$key = $value;
	}

	
}
