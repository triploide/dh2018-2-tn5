<?php

class Usuario extends Modelo
{
	public $key = 'usuarios';

	protected $nombre;
	protected $apellido;
	protected $email;
	protected $pass;

	public function __construct($email, $pass)
	{
		parent::__construct();
		$this->email = $email;
		$this->pass = $pass;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getPass()
	{
		return $this->pass;
	}
	
}
