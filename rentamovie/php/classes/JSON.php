<?php

class JSON extends Base
{
	public function seleccionar($attr, $valor, $modelo)
	{
		$modelos = file_get_contents( '../../' . $this->archivo);
		$modelos = json_decode($modelos, true);
		$modelos = $modelos[$modelo->key];

		foreach ($modelos as $dato) {
			if ($dato[$attr] == $valor) {
				return $modelo->meterAtributos($dato);
			}
		}
		return false;
	}

	public function crear() {

	}

	public function modificar() {

	}

	public function borrar() {

	}
}
