<?php

class SESSION extends Base
{
	public function seleccionar($attr, $valor, $modelo)
	{
		$modelos = $_SESSION[$modelo->key];
		foreach ($modelos as $dato) {
			if ($dato[$attr] == $valor) {
				return $dato;
			}
		}

		return false;
	}

	public function crear() {

	}

	public function modificar() {

	}

	public function borrar() {

	}
}
