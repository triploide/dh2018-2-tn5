<?php

abstract class Base
{
	protected $key;
	protected $archivo = 'db/db.json';

	public abstract function seleccionar($attr, $valor, $modelo);

	public abstract function crear();

	public abstract function modificar();

	public abstract function borrar();
}
