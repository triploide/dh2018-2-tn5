<?php
$usuarios = [
	'bort@mail.com' => [
		'nombre' => 'Bort',
		'email' => 'bort@mail.com',
        'pass' => '$2y$10$keV8EeM29DPAJzj3FtZwZ.yA7GO6z.FoyltYK8ytmf39fzfazJE1O',
		'avatar' => 'bort.png',
	]
];

$usuario = $usuarios[$_POST['email']];

if (!$usuario) {
    $_SESSION['errors'][] = 'El password o el mail ingresado no es correcto.';
    header('Location: '.$CONFIG['url'].'login.php');
    exit;
}

if (!password_verify($_POST['password'], $usuario['pass'])) {
    $_SESSION['errors'][] = 'El password o el mail ingresado no es correcto.';
    header('Location: '.$CONFIG['url'].'login.php');
    exit;
}

