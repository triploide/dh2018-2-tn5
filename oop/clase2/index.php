<?php
require 'classes/Persistible.php';
require 'classes/Producto.php';
require 'classes/Accesorio.php';
require 'classes/Ropa.php';

$remera = new Ropa('Remera', 60, 300, 'M');
$mochila = new Accesorio('Mochila', 40, 1500);

$productos = [$remera, $mochila];

foreach ($productos as $producto) {
  $producto->guardar();
}
