<?php
  require 'classes/Persistible.php';
  require 'classes/Producto.php';
  require 'classes/Ropa.php';
  require 'classes/Talle.php';

  $remera = new Ropa('Remera', 60, 200, 'm');
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Cargar Producto</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <style>
      body {
        padding: 40px
      }
  </style>
</head>
<body>
  <div class="container">
    <h1>Cargar Producto</h1>
    <form action="" method="post">
      <div class="form-group">
        <label for="nombre">Nombre</label>
        <input type="email" class="form-control" id="nombre">
      </div>
      <div class="form-group">
        <label for="costo">Costo</label>
        <input type="number" class="form-control" id="costo">
      </div>
      <div class="form-group">
        <label for="margen">Margen</label>
        <input type="number" class="form-control" id="margen">
      </div>
      <div class="form-group">
        <label for="talle">Talle</label>
        <?php echo Talle::toSelect($remera); ?>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Enviar</button>
      </div>
    </form>
  </div>
</body>
</html>
