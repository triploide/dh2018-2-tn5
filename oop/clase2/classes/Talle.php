<?php

class Talle
{
  public static $valores = [
    's' => 'Pequeño',
    'm' => 'Mediano',
    'l' => 'Grande'
  ];

  public static function toSelect($producto = null)
  {
    $talle = ($producto) ? $producto->getTalle() : '';
    $html  = '<select class="form-control" id="talle">';
    foreach (self::$valores as $key => $value) {
      $selected = ($talle == $key) ? 'selected' : '';
      $html .= '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
    }
    $html .= '</select>';
    return $html;
  }
}
