<?php

class Ropa extends Producto
{
  private $talle;

  public function __construct($nombre, $margen, $costo, $talle)
  {
    parent::__construct($nombre, $margen, $costo);
    $this->talle = $talle;
  }

  public function getMargen()
  {
    return $this->margen;
  }

  public function getPrecio()
  {
    $total = parent::getPrecio();
    return $total * 1.21;
  }

  public function getTalle()
  {
    return $this->talle;
  }

  public function getPrecioPromocional()
  {
    return $this->getPrecio();
  }

  public function guardar()
  {
    echo "Guardando $this->nombre";
  }
}
