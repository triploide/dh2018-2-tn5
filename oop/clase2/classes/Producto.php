<?php

abstract class Producto implements Persistible
{
  protected $nombre;
  protected $margen;
  protected $costo;

  public function __construct($nombre, $margen, $costo)
  {
    $this->nombre = $nombre;
    $this->margen = $margen;
    $this->costo = $costo;
  }

  protected function getPrecio()
  {
    return $this->costo + ($this->margen * $this->costo / 100);
  }

  abstract public function getPrecioPromocional();

}
