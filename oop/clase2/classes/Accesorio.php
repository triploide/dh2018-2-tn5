<?php

class Accesorio extends Producto
{
  public function getPrecioPromocional()
  {
    return $this->getPrecio() * 0.8;
  }

  public function guardar()
  {
    echo "Guardando $this->nombre";
  }
}
