<?php

class Persona
{
  private $nombre;
  private $edad;
  private $email;
  private $password;

  public function __construct($nombre, $edad, Email $email, $password)
  {
    $this->nombre = $nombre;
    $this->edad = $edad;
    $this->email = $email->getEmail();
    $this->setPassword($password);
  }

  public function getEdad()
  {
    return $this->edad;
  }

  public function setEdad($edad)
  {
    $this->edad = $edad;
  }

  public function getPassword()
  {
    $isAdmin = false;
    if ($isAdmin) {
      return $this->password;
    } else {
      return null;
    }
  }

  public function setPassword($password)
  {
    $this->password = password_hash($password, PASSWORD_DEFAULT);
  }

  public function saludar()
  {
    echo "Hola soy " . $this->nombre;
  }
}
