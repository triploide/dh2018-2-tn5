<?php
declare(strict_types = 1);

function sumar(int $a, int $b) : int
{
  return $a + $b;
}

$a = sumar(3, 5);
$b = sumar(2, 7);

echo sumar($a, $b);
