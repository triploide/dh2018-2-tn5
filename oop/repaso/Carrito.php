<?php

class Carrito
{
	private $productos;
	private $cupon;

	public function aplicarCupon($cupon)
	{
		$this->cupon = $cupon;
	}

	public function getTotal()
	{
		/*
		$total = 0;
		foreach ($this->productos as $producto) {
			$total += $producto->getTotal();
		}
		return $total;
		*/
		$total = 500;
		if ($this->cupon) {
			return $this->cupon->getTotal($total);
		} else {
			return $total;
		}
	}
}
