<?php

class Cupon
{
	private $descuento;

	public function __construct($descuento)
	{
		$this->descuento = $descuento;
	}

	public function getTotal($total)
	{
		return $total - $this->descuento;
	}

}