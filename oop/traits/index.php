<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Usuario</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h1>Cargar usuario</h1>
		<form action="controllers/usuario.controller.php" method="post" enctype="multipar/form-data">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" class="form-control" id="nombre" name="nombre">
			</div>
			<div class="form-group">
				<label for="imagen">Imagen</label>
				<input type="file" class="form-control" id="imagen" name="imagen">
			</div>
			<div class="form-group">
				<input class="btn btn-primary" type="submit" name="enviador">
			</div>
		</form>
	</div>
</body>
</html>