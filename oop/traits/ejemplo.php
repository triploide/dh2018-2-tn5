<?php

require 'classes/Imagen.php';
require 'classes/Nombrable.php';
require 'classes/Usuario.php';
require 'classes/Producto.php';

$usuario = new Usuario('Bort');
$usuario->agregarImagen();
echo $usuario->getImagen();

echo '<br>';

$producto = new Producto(200);
$producto->agregarImagen();
echo $producto->getImagen();
