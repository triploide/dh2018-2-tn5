<?php

trait Imagen
{

	private $imagen;

	public function getImagen()
	{
		return $this->imagen;
	}

	public function setImagen($imagen)
	{
		$this->imagen = $imagen;
	}

	public function agregarImagen($name, $path)
	{
		$ext = pathinfo($_FILES[$name]['name'], PATHINFO_EXT);
		$nombre = $this->getNombre() . '.' . $ext;
		move_uploaded_file($_FILES[$name]['tmp_name'], $path . $nombre);
		$this->setImagen($nombre);
	}
}
