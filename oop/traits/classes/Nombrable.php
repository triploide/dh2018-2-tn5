<?php

interface Nombrable
{
	public function getNombre();
}