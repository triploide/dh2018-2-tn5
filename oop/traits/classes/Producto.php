<?php

class Producto implements Nombrable
{
	use Imagen;

	private $precio;

	public function __construct($precio)
	{
		$this->precio = $precio;
	}

	public function getPrecio()
	{
		return $this->precio;
	}

	public function getNombre()
	{
		return '';
	}

}
