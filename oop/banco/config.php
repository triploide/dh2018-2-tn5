<?php

require_once 'Interfaces/liquidable.php';
require_once 'Interfaces/imprimible.php';
require_once 'Cuentas/cuenta.php';
require_once 'Cuentas/black.php';
require_once 'Cuentas/classic.php';
require_once 'Cuentas/gold.php';
require_once 'Cuentas/platinum.php';
require_once 'Clientes/cliente.php';
require_once 'Clientes/multinacional.php';
require_once 'Clientes/persona.php';
require_once 'Clientes/pyme.php';

